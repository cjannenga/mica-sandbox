import { createAction, props } from '@ngrx/store';

export const changeApp = createAction('[Global] App Changed', props<{ app: string; }>());
