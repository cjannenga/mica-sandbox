import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreatmentPoliciesComponent } from './treatment-policies.component';

describe('TreatmentPoliciesComponent', () => {
  let component: TreatmentPoliciesComponent;
  let fixture: ComponentFixture<TreatmentPoliciesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TreatmentPoliciesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreatmentPoliciesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
