import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrugWizardComponent } from './drug-wizard.component';

describe('DrugWizardComponent', () => {
  let component: DrugWizardComponent;
  let fixture: ComponentFixture<DrugWizardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrugWizardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrugWizardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
