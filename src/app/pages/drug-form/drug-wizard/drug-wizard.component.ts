import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatStepper } from '@angular/material/stepper';

export interface WeightRange {
  value: string;
}

export interface Drug {
  name: string;
  activeIngredient: string;
  genericName: string;
  atc: string;
}



@Component({
  selector: 'app-drug-wizard',
  templateUrl: './drug-wizard.component.html',
  styleUrls: ['./drug-wizard.component.sass']
})
export class DrugWizardComponent implements OnInit {

  @ViewChild('stepper') stepper: MatStepper;
  drugGroup: FormGroup;
  sourceGroup: FormGroup;
  policyGroup: FormGroup;
  dosageGroup: FormGroup;
  drugSearched = false;
  loading = false;
  currentDrugResults: Drug[] = [];
  proposedDrug: Drug;

  weightRanges: WeightRange[] = [
    {value: '0 lbs - 20 lbs'},
    {value: '21 lbs - 50 lbs'},
    {value: '51 lbs - 100 lbs'},
    {value: '101 lbs - 150 lbs'},
    {value:  '+ 151 lbs'}
  ];

  drugs: Drug[] = [
    { name: 'Advil', activeIngredient: 'Ibuprofen', atc: 'AAB401A', genericName: 'Ibuprofen' },
    { name: 'Advil PM', activeIngredient: 'Ibuprofen', atc: 'AAB501A', genericName: 'Ibuprofen' },
    { name: 'Advil Severe', activeIngredient: 'Ibuprofen', atc: 'AAB501B', genericName: 'Ibuprofen Severe' }
  ];

  ngOnInit() {
    this.drugGroup = this.fb.group({
      drugNameCtrl: ['', Validators.required]
    });
    this.sourceGroup = this.fb.group({
      sourceUrlCtrl: ['', Validators.required],
      sourceTextCtrl: ['', Validators.required],
      sourceTypeCtrl: ['', Validators.required]
    });
    this.policyGroup = this.fb.group({
      policyCtrl: ['', Validators.required]
    });
    this.dosageGroup = this.fb.group({
      dosageCtrl: ['', Validators.required]
    });
  }

  constructor(private fb: FormBuilder,
              public dialogRef: MatDialogRef<DrugWizardComponent>) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  searchDrug(input: string) {
    this.loading = true;
    this.currentDrugResults = this.drugs.filter(x =>  x.activeIngredient === input);
    setTimeout(() => {
      this.loading = false;
      this.drugSearched = true;
    }, 2000);
  }

  setDrug(drug: Drug) {
    this.proposedDrug = drug;
    this.stepper.next();
  }

  editStep(index: number) {
    this.stepper.selectedIndex = index;
  }

}

