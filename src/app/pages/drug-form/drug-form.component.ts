import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DrugWizardComponent } from './drug-wizard/drug-wizard.component';

@Component({
  selector: 'app-drug-form',
  templateUrl: './drug-form.component.html',
  styleUrls: ['./drug-form.component.sass']
})
export class DrugFormComponent implements OnInit {

  constructor(private dialog: MatDialog) {}

  ngOnInit() {

  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DrugWizardComponent, {
      width: '100%',
      maxHeight: '80%'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

}


