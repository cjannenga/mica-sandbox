import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreatmentInspectorComponent } from './treatment-inspector.component';

describe('TreatmentInspectorComponent', () => {
  let component: TreatmentInspectorComponent;
  let fixture: ComponentFixture<TreatmentInspectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TreatmentInspectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreatmentInspectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
