import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavigationComponent } from './layout/navigation/navigation.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { TreatmentTasksComponent } from './pages/treatment-tasks/treatment-tasks.component';
import { TreatmentInspectorComponent } from './pages/treatment-inspector/treatment-inspector.component';
import { TreatmentPoliciesComponent } from './pages/treatment-policies/treatment-policies.component';
import { StoreModule } from '@ngrx/store';
// import { reducers, metaReducers } from './reducers';
import * as fromApp from './reducers/app.reducer';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { DrugFormComponent } from './pages/drug-form/drug-form.component';
import { MatStepperModule } from '@angular/material/stepper';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DrugWizardComponent } from './pages/drug-form/drug-wizard/drug-wizard.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatCardModule } from '@angular/material/card';
import { MatSelectModule } from '@angular/material/select';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    TreatmentTasksComponent,
    TreatmentInspectorComponent,
    TreatmentPoliciesComponent,
    DrugFormComponent,
    DrugWizardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatMenuModule,
    MatStepperModule,
    MatInputModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatDividerModule,
    MatCardModule,
    MatProgressSpinnerModule,
    StoreModule.forRoot({app: fromApp.reducer}),
    StoreDevtoolsModule.instrument({maxAge: 25, logOnly: environment.production}),
    MatSelectModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [ DrugWizardComponent ]
})
export class AppModule { }
