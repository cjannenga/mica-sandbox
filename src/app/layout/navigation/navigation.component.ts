import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable, BehaviorSubject } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { Store, select } from '@ngrx/store';
import { changeApp } from '../../actions/app.actions';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {
  theme = new BehaviorSubject<string>('treatments');
  app$: Observable<string>;
  currentApp: string;

  links: any = {
    treatments : [{
      route: '/tasks',
      title: 'Tasks'
    },
    {
      route: '/inspect',
      title: 'Inspector'
    },
    {
      route: '/policies',
      title: 'Policies'
    }],
    mica: [{
      route: '/',
      title: 'Symptoms'
    },
      {
        route: '/',
        title: 'Templates'
      },
      {
        route: '/',
        title: 'Inspector'
      },
      {
        route: '/',
        title: 'Groups'
      }],
    mita: [{
      route: '/',
      title: 'Summary'
    },
      {
        route: '/',
        title: 'Users'
      },
      {
        route: '/',
        title: 'Collectors'
      },
      {
        route: '/',
        title: 'Reviewers'
      },
      {
        route: '/',
        title: 'Tasks'
      },
      {
        route: '/',
        title: 'Tracking'
      },
      {
        route: '/',
        title: 'Assignments'
      },
      {
        route: '/',
        title: 'Data-frame Generation'
      }]
  };



  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver, private store: Store<{app: string}>) {
    this.app$ = store.pipe(select('app'));
  }

  ngOnInit() {
    this.app$.subscribe((app: any) => {
      this.currentApp = app.app;
    });
  }

  currentTheme() {
    return this.theme.asObservable();
  }

  getApp() {

    return this.links[this.currentApp];
  }



  changeApp(app: string) {
    switch (app) {
      case 'mica':
        this.store.dispatch(changeApp({app: 'mica'}));
        break;
      case 'treatments':
        this.store.dispatch(changeApp({ app: 'treatments' }));
        break;
      case 'mita':
        this.store.dispatch(changeApp({ app: 'mita' }));
        break;
      default:
        this.store.dispatch(changeApp({ app: '' }));
    }
  }

}
