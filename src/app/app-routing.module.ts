import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TreatmentTasksComponent } from './pages/treatment-tasks/treatment-tasks.component';
import { TreatmentInspectorComponent } from './pages/treatment-inspector/treatment-inspector.component';
import { TreatmentPoliciesComponent } from './pages/treatment-policies/treatment-policies.component';
import { DrugFormComponent } from './pages/drug-form/drug-form.component';


const routes: Routes = [
  { path: '', pathMatch: 'full', component: DrugFormComponent },
  { path: 'tasks', component: TreatmentTasksComponent },
  { path: 'inspect', component: TreatmentInspectorComponent },
  { path: 'policies', component: TreatmentPoliciesComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
