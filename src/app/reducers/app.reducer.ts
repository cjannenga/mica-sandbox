import { Action, createReducer, on } from '@ngrx/store';
import { changeApp } from '../actions/app.actions';

export interface State {
    app: string;
}

export const initialState: State = {
    app: ''
};


const appReducer = createReducer(initialState,
    on(changeApp, (state, { app }) =>  ({ app }))
    );

export function reducer(state: State | undefined, action: Action) {
        return appReducer(state, action);
    }
